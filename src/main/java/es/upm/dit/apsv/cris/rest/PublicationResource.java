package es.upm.dit.apsv.cris.rest;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.ArrayList;

import javax.json.JsonArray;
import javax.json.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import es.upm.dit.apsv.cris.dao.ResearcherDAOImplementation;
import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.pubsub.v1.stub.GrpcSubscriberStub;
import com.google.cloud.pubsub.v1.stub.SubscriberStub;
import com.google.cloud.pubsub.v1.stub.SubscriberStubSettings;
import com.google.pubsub.v1.AcknowledgeRequest;
import com.google.pubsub.v1.ProjectSubscriptionName;
import com.google.pubsub.v1.PullRequest;
import com.google.pubsub.v1.PullResponse;
import com.google.pubsub.v1.ReceivedMessage;

@Path("/Publications")
public class PublicationResource {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Publication> readAll(){
		return PublicationDAOImplementation.getInstance().readAll();
	}
	
	//CRUD (create, read, update, delete)
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(Publication pnew) throws URISyntaxException{
		Publication p = PublicationDAOImplementation.getInstance().create(pnew);
		URI uri = new URI("/CRISSERVICE/rest/Publications/"+p.getId());
		return Response.created(uri).build();
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response read(@PathParam("id") String id) {
		Publication p = PublicationDAOImplementation.getInstance().read(id);
		if (p == null)
			return Response.status(Response.Status.NOT_FOUND).build();
		return Response.ok(p, MediaType.APPLICATION_JSON).build();
		}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON) //this come with the response
	@Path("{id}")
	public Response update(@PathParam("id") String id, Publication p) {
		Publication pold = PublicationDAOImplementation.getInstance().read(id);
		if ((pold == null)|| (! pold.getId().contentEquals(p.getId())))
			return Response.notModified().build();
		PublicationDAOImplementation.getInstance().update(p);
		return Response.ok().build();
	}
	
	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") String id) {
		Publication pold = PublicationDAOImplementation.getInstance().read(id);
		if (pold == null)
			return Response.notModified().build();
		PublicationDAOImplementation.getInstance().delete(pold);
		return Response.ok().build();
	}
	
	//Synchronous access to external Scopus REST API and update citationNumber
	@GET
	@Path("{id}/UpdateCiteNumber")
	public Response updateCiteNumber(@PathParam("id") String id) {
		String APIKey = "3d60deb70371c42a7b054e9566c1410c";
		Publication p = PublicationDAOImplementation.getInstance().read(id);
		if (p == null)
			return Response.status(Response.Status.NOT_FOUND).build();
		Client client = ClientBuilder.newClient(new ClientConfig());
		JsonObject o = client.register(JsonProcessingFeature.class).target("https://api.elsevier.com/content/search/scopus?query=SCOPUS-ID("
				+ id + ")&field=citedby-count")
			.request().accept(MediaType.APPLICATION_JSON)
			.header("X-ELS-APIKey", APIKey)
			.get(JsonObject.class);
		
		
		try {
		JsonArray a = ((JsonArray) ((JsonObject) o.getJsonObject("search-results")).get("entry"));
		JsonObject oo = a.getJsonObject(0);
		int ncites = Integer.parseInt(oo.get("citedby-count").toString().replaceAll("\"", ""));
		
		p.setCiteCount(ncites);
		PublicationDAOImplementation.getInstance().update(p);
		
		}catch (Exception e) {
			
		}
		
		return Response.ok().build();
		
		
	//
		
	}
}
